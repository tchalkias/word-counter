package gr.saras;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The class that contains the entry point of the application.
 */
public class Main {

    /**
     * The entry point of the application.
     *
     * @param args The command line arguments:
     *             - `args[0]`: the file to be checked
     *             - `args[1]` (optional): the word to search for
     *             <p>
     *             Example call:
     *             ```
     *             java word-counter my-file.txt hello
     *             ```
     *             <p>
     *             In the above call:
     *             - `args[0]` = my-file.txt
     *             - `args[1]` = hello
     */
    public static void main(String[] args) {
        Map<String, String> parsedArgs = parseCommandLineArguments(args);
        Map<String, Number> wordsCountMap = countWords(parsedArgs.get("filePath"));
        printWordsCount(wordsCountMap, parsedArgs.get("searchWord"));
    }

    private static Map<String, Number> countWords(String filePath) {
        // A map where:
        // - the keys are the words in the file and
        // - the values are the number of times each word appears in the file.
        //
        // For example:
        // {
        //   "hello": 3,
        //   "the": 5,
        //   ...
        // }
        //
        // This map will get a value in the `try` block below, so we could skip initializing it in an empty map here.
        // However, an error might occur while reading the file. In that case, this map would end up uninitialized and
        // we want to avoid this scenario.
        Map<String, Number> wordsCountMap = new HashMap<>();

        // Get the real file path.
        //
        // For example, the user may have given:
        // - "./my-file.txt"
        // - "my-file.txt"
        // - "../my-file.txt"
        // - "C:\My Documents\my-file.txt"
        //
        // Here, this path is resolved into a full path, like: "C:\My Documents\my-file.txt".
        Path path = Paths.get(filePath);

        // Read the file per line.
        try (Stream<String> fileStream = Files.lines(path)) {
            // for each line:
            wordsCountMap = fileStream
                    // 1. split the line resulting in an array of the line's words
                    .map((String line) -> line.split("\\P{L}+"))

                    // 2. map the array of the line's words into a wordsCountMap only for this line
                    .map((String[] lineWords) -> {
                        Map<String, Number> lineWordsCountMap = new HashMap<>();
                        int wordCount;

                        // 2.1 for each word in the current line:
                        for (String word : lineWords) {
                            // 2.2 check if word exists in current lineWordsCountMap
                            boolean wordInCountMap = lineWordsCountMap.containsKey(word);

                            // 2.3 Word Count Initialization
                            wordCount = wordInCountMap
                                    // 2.3.1 if the word is in the lineWordsCountMap, then read the word's current count
                                    //       from the map
                                    ? (int) lineWordsCountMap.get(word)
                                    // 2.3.2 else initialize its counter to 0
                                    : 0;

                            // 2.4 increase the words counter by one
                            wordCount++;

                            // 2.5 update the word's count in the lineWordsCountMap
                            lineWordsCountMap.put(word, wordCount);
                        }

                        // 2.6 return the current line's word count map
                        return lineWordsCountMap;
                    })

                    // 3. get the stream of entries in the lineWordsCountMap (just for Java to work, not related to the
                    //    algorithm)
                    .flatMap((Map<String, Number> lineWordsCountMap) -> lineWordsCountMap.entrySet().stream())

                    // 4. merge the lineWordsCountMaps of all lines
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue,
                            (Number map1WordCount, Number map2WordCount) -> map1WordCount.intValue() + map2WordCount.intValue()
                    ));
        } catch (IOException e) {
            System.out.println("The following error occurred while reading the file.");
            e.printStackTrace();
        }

        return wordsCountMap;
    }

    /**
     * A helper method used only for extracting the file path and the search word from the command line arguments.
     *
     * @param args The command line arguments.
     * @return A map containing the parsed command line arguments.
     */
    private static Map<String, String> parseCommandLineArguments(String[] args) {
        Map<String, String> parseArgs = new HashMap<>();

        parseArgs.put("filePath", args[0]);
        parseArgs.put("searchWord", args.length == 2 ? args[1] : "");

        return parseArgs;
    }

    private static void printWordsCount(Map<String, Number> wordsCountMap, String searchWord) {
        // Print all words and their count
        for (Map.Entry<String, Number> entry : wordsCountMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

        // If no searchWord has been specified, then we are done
        if (searchWord.isEmpty()) {
            return;
        }

        // Else, display the search word's count if the search word was in the file, otherwise display a "not found"
        // message.
        String searchWordMessage = wordsCountMap.containsKey(searchWord)
                ? "The word \"" + searchWord + "\" was found " + wordsCountMap.get(searchWord) + " times"
                : "\"" + searchWord + "\" was not found in the file.";
        System.out.println(); // blank line for style
        System.out.println(searchWordMessage);
    }
}
