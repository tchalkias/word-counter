# Οδηγίες

1. κατέβασε το [Java Development Kit v11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html) (jdk-11.0.2_windows-x64_bin.exe)
2. κατέβασε το project
     1. πάτα [Downloads](https://bitbucket.org/tchalkias/word-counter/downloads/) στο μενού αριστερά
     2. πάτα [Download repository](https://bitbucket.org/tchalkias/word-counter/get/9359f86b7f78.zip)
3. αποσυμπίεσε το zip που θα κατέβει

## Κώδικας

Ο κώδικας βρίσκεται στον φάκελο `src`.

## Εκτελέσιμο

Το εκτελέσιμο (word-counter.jar) βρίσκεται στον φάκελο `test`.

## Παραδείγματα

Έχω βάλει δύο δοκιμαστικά αρχεία:

- test.txt
- test-greek.txt

Για να τα τρέξεις:

1. άνοιξε ένα cmd στον φάκελο `test`

2. γράψε:

   ```
   java -jar word-counter.jar <το path για το αρχείο> <τη λέξη που ψάχνεις (προαιρετική)>
   ```

### Παράδειγμα 1

Βρίσκει πόσες φορές εμφανίζονται όλες οι λέξεις.

```
java -jar word-counter.jar test.txt
```

### Παράδειγμα 2

Παρόμοια με το "Παράδειγμα 1", αλλά στο τέλος σου εμφανίζει συγκεκριμένα για τη λέξη "Nullam" πόσες φορές εμφανίζεται.

```
java -jar word-counter.jar test.txt Nullam
```

### Παράδειγμα 3

Ίδιο με το 2, αλλά σε ελληνικό κείμενο.

```
java -jar word-counter.jar test-greek.txt να
```

